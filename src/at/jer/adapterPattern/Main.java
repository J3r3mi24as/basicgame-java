package at.jer.adapterPattern;

import at.jer.basicGame.basicGame;
import org.newdawn.slick.*;

public class Main extends BasicGame {

    private Actor rect;

    public Main(String title) {
        super(title);
    }

    public static void main(String[] args) {
        try {
            AppGameContainer container = new AppGameContainer(new Main("Test"));
            container.setDisplayMode(800,600,false);
            container.setTargetFrameRate(60);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.rect = new Adapter(new Rect());
    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        this.rect.update(gameContainer, i);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        this.rect.render(gameContainer, graphics);
    }
}
