package at.jer.adapterPattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Adapter implements Actor {
    GameContainer gc;
    Graphics g;
    int delta;
    Legacy oldActor;

    public Adapter(Legacy oldActor) {
        this.oldActor = oldActor;
    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        this.gc = gameContainer;
        this.delta = i;
        oldActor.move(this.gc);

    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        this.g = graphics;
        this.gc = gameContainer;
        oldActor.paint(this.g, this.delta);
    }

}
