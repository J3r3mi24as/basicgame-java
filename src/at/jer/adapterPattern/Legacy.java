package at.jer.adapterPattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Legacy {
    void move(GameContainer gc); //update
    void paint(Graphics g, int delta); //render
}
