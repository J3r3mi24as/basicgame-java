package at.jer.adapterPattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Rect implements Legacy {

    float x,y;

    @Override
    public void move(GameContainer gc) {
        this.x++;
        gc.pause();
        gc.resume();
    }

    @Override
    public void paint(Graphics g, int delta) {
        g.drawRect(this.x, this.y, 10, 10);

    }
}
