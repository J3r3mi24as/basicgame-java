package at.jer.wrdGäs;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;

public class LifesCounter {
    private int lifesLeft;
    private Image cat;
    private Image mouse;
    private float catX;
    private float catY;
    private float mouseX;
    private float mouseY;

    public int getLifesLeft() {
        return lifesLeft;
    }

    public LifesCounter() throws SlickException {
        this.lifesLeft = 8;
        this.cat = new Image("src/at/jer/wrdGäs/cat.png").getScaledCopy(100, 60);
        this.mouse = new Image("src/at/jer/wrdGäs/mouse.png").getScaledCopy(100, 60);
        this.catX = 100;
        this.catY = 400;
        this.mouseX = 600;
        this.mouseY = 400;
    }



    public void setLifesLeft(int lifesLeft) {
        this.lifesLeft = lifesLeft;
    }

    public void render(Graphics graphics) {
        graphics.drawImage(cat, catX, catY);
        graphics.drawImage(mouse, mouseX, mouseY);
    }

    public void update(int delta) {
        int lifesUsed = 8 - this.lifesLeft;

        this.catX = 100 + (lifesUsed * 50);
    }
}
