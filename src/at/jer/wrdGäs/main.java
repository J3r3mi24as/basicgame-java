package at.jer.wrdGäs;
import org.newdawn.slick.*;

public class main {
    public static void main(String[] args) {
        try {
            AppGameContainer container = new AppGameContainer(new Game("Hangman"));
            container.setDisplayMode(800,600,false);
            container.setTargetFrameRate(200);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
