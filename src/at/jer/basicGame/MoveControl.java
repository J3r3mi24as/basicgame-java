package at.jer.basicGame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public class MoveControl implements MoveStrategy {

    private float x,y, speed;
    private CircleActor oval;


    public void setOval(CircleActor circle) {
        this.oval = circle;
    }

    public MoveControl(float x, float y, float speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    @Override
    public void update(GameContainer gc, int delta) {
        this.move(gc, delta);

    }

    public Boolean isIntersecting() {
        if(this.x < 0 || this.x > 800) {
            return true;
        }
        else return false;
    }

    @Override
    public float getX() {
        return this.x;
    }

    @Override
    public float getY() {
        return this.y;
    }


    @Override
    public void setX(float x) {
        this.x = x;
    }

    @Override
    public void setY(float y) {
        this.y = y;
    }

    public void move(GameContainer gc, int delta) {
        if(gc.getInput().isKeyDown(Input.KEY_LEFT)) {
            this.x -= (float) delta * speed;
            System.out.println("left: x "+ this.x);
        }
        if(gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
            this.x += (float) delta * speed;
            System.out.println("right: x "+ this.x);
        }

    }
}
