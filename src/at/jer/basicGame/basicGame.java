package at.jer.basicGame;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

public class basicGame extends BasicGame {

    private CircleActor ca1;
    private List<Actor> actors;


    public basicGame(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();
        this.actors.add(new RectActor(new MoveLeft(800, 400, 0.5f)));
        this.actors.add(new CircleActor(new MoveControl(0,0, 0.3f)));
        this.actors.add(new CircleActor(new MoveLeft(800,200, 0.4f)));
        for (Actor actor: this.actors) {
            actor.init(gameContainer);
        }
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        for (Actor actor: this.actors) {
            actor.update(gc, delta);
            MoveStrategy ms = actor.getMs();
            if(ms.isIntersecting()) {
                for (Actor actor2: this.actors) {
                    actor2.inform();
                }
            }
        }

    }

    @Override
    public void render(GameContainer gc, Graphics graphics) throws SlickException {
        for (Actor actor: this.actors) {
            actor.render(gc, graphics);
        }
    }

    public static void main(String[] args) {
        System.out.println("Test");
        try {
            AppGameContainer container = new AppGameContainer(new basicGame("Test"));
            container.setDisplayMode(800,600,false);
            container.setTargetFrameRate(200);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }

    }
}
