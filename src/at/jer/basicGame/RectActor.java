package at.jer.basicGame;

import org.newdawn.slick.*;

public class RectActor implements Actor {

    private MoveStrategy ms;
    private Color color;

    public RectActor(MoveStrategy ms) {
        this.ms = ms;
        this.color = Color.white;
    }

    public MoveStrategy getMs() {
        return ms;
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {

    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException {
        this.ms.update(gc, i);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.setColor(this.color);
        graphics.drawRect(this.ms.getX(), this.ms.getY(), 50, 50);
    }

    public void inform() {
        this.color = Color.red;
        this.ms.setX(300);
    }
}
