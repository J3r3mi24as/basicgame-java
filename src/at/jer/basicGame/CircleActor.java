package at.jer.basicGame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class CircleActor implements Actor {
    private MoveStrategy ms;
    private Color color;

    public CircleActor( MoveStrategy ms) {
        this.ms = ms;
        this.color = Color.white;
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
    }

    public void update(GameContainer gc, int delta) {
        this.ms.update(gc, delta);
    }

    public MoveStrategy getMs() {
        return ms;
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.setColor(this.color);
        graphics.drawOval(this.ms.getX(), this.ms.getY(), 50, 50);
    }

    public void inform() {
        this.color = Color.green;
        this.ms.setX(300);
    }


}
