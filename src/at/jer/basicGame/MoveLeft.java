package at.jer.basicGame;

import org.newdawn.slick.GameContainer;

public class MoveLeft implements MoveStrategy {
    private float x,y, speed;

    public MoveLeft(float x, float y, float speed) {
        super();
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void update(GameContainer gc, int delta) {

        this.x -= delta * this.speed;
    }

    public Boolean isIntersecting() {
        return false;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }


    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
