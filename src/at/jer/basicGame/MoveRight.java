package at.jer.basicGame;

import org.newdawn.slick.GameContainer;

public class MoveRight implements MoveStrategy {
    private float x,y, speed;

    public MoveRight(float x, float y, float speed) {
        super();
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void update(GameContainer gc, int delta) {
        this.x += delta * this.speed;
    }

    public float getX() {
        return x;
    }

    @Override
    public void setX(float x) {
        this.x = x;
    }

    @Override
    public void setY(float y) {
        this.y = y;
    }

    public Boolean isIntersecting() {
        return false;
    }

    public float getY() {
        return y;
    }
}
