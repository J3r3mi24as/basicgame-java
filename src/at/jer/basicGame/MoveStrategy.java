package at.jer.basicGame;

import org.newdawn.slick.GameContainer;

public interface MoveStrategy {
    void update(GameContainer gc, int delta);

    float getX();

    float getY();

    void setX(float x);

    void setY(float y);

    Boolean isIntersecting();
}
