package at.jer.Maturaufgabe1;

import java.util.ArrayList;
import java.util.List;

public class Thermometer {

    private int temp;
    private List<Observer> listOfObserver;

    public Thermometer() {
        temp = 0;
        this.listOfObserver = new ArrayList<>();
    }

    public void checkTemp() {
        if(temp > 30) {
            for (Observer el: this.listOfObserver) {
                el.info();
            }
        }
    }

    public void setObserver(Observer  ob) {
        this.listOfObserver.add(ob);
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }
}
