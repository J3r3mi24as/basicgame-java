package at.jer.Maturaufgabe1;

import java.util.ArrayList;

public class Haussteuerung{


    public static void main(String[] args) {
        Thermometer t1 = new Thermometer();

        t1.setObserver(new Heizregler());
        t1.setObserver(new Markisenregler());
        int i = 0;
        while(true) {
            i++;
            t1.checkTemp();
            t1.setTemp(i);
            System.out.println(i);
            if(i > 35) {
                break;
            }
        }




    }
}
