package at.jer.Maturaaufgabe2;

public class Engine {
    public static void main(String[] args) {
        InjectionSystem is1 = new InjectionSystem(new African());

        InjectionSystem is2 = new InjectionSystem(new HighAlpin());

        is1.inject();
        is2.inject();

        is1.eco();
        is2.eco();

        is1.inject();
        is2.inject();


    }
}
