package at.jer.Maturaaufgabe2;

public class HighAlpin implements InjectionStrategy {
    private int mode;
    private static int MULTIPLIER = 10;

    public HighAlpin() {
        this.mode = 10;
    }

    @Override
    public void inject() {
        int gas = this.mode * this.MULTIPLIER;
        System.out.println("High Alpin: Burned Gas:" + gas);
    }

    @Override
    public void setMode(int i) {
        this.mode = i;
    }
}
