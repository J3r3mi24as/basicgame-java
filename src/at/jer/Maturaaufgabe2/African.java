package at.jer.Maturaaufgabe2;

public class African implements InjectionStrategy {
    private int mode;
    private static int MULTIPLIER = 3;

    public African() {
        this.mode = 10;
    }

    @Override
    public void inject() {
        int gas = this.mode * this.MULTIPLIER;
        System.out.println("African Burned Gas:" + gas);
    }

    @Override
    public void setMode(int i) {
        this.mode = i;
    }
}
