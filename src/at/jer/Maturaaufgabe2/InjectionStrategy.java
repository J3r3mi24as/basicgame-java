package at.jer.Maturaaufgabe2;

public interface InjectionStrategy {
    void inject();
    void setMode(int i);
}
