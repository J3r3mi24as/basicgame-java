package at.jer.Maturaaufgabe2;

public class InjectionSystem {
    private InjectionStrategy injectionStrategy;
    public InjectionSystem(InjectionStrategy is1) {
        this.injectionStrategy = is1;
    }

    public void inject() {
        this.injectionStrategy.inject();
    }

    public void eco() {
        this.injectionStrategy.setMode(3);
    }
}
