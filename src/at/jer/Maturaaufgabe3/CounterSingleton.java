package at.jer.Maturaaufgabe3;

public class CounterSingleton {

    private static CounterSingleton instance = null;
    private int counter;

    public CounterSingleton() {
        this.counter = 0;
    }

    public static CounterSingleton getInstance() {
        if(instance == null) {
            instance = new CounterSingleton();
        }
        return instance;
    }

    public void increaseCounter() {
        this.counter++;

    }

    public int getCounter() {
        return this.counter;
    }

}
