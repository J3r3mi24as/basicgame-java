package at.jer.Maturaaufgabe3;


public class Main {
    public static void main(String[] args) {
        String os1 = "OSX";
        String os2 = "Windows";
        String os3 = "Linux";
        Algorithm algo1 = AlgoFactory.getBestAlgorithm(os1);
        algo1.encrypt("Test for OSX");
        Algorithm algo2 = AlgoFactory.getBestAlgorithm(os2);
        algo2.encrypt("Test for Windows");
        Algorithm algo3 = AlgoFactory.getBestAlgorithm(os3);
        algo3.encrypt("Test for Linux");

        CounterSingleton cs1 = CounterSingleton.getInstance();
        cs1.increaseCounter();
        System.out.println(cs1.getCounter());

        CounterSingleton cs3 = CounterSingleton.getInstance();

        cs3.increaseCounter();;
        System.out.println(cs3.getCounter());
        System.out.println(cs1.getCounter());
    }
}
