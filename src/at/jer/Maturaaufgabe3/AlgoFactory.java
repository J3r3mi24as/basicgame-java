package at.jer.Maturaaufgabe3;

public class AlgoFactory {
    public static Algorithm getBestAlgorithm(String os) {
        if(os.equals("OSX")) {
            return new SteveJobsAlgo();
        }

        if(os.equals("Windows")) {
            return new BillGatesAlgo();
        }

        if(os.equals("Linux")) {
            return new LinusTorvaldsAlgo();
        }
        return null;
    }
}
