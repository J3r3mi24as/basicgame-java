package at.jer.Maturaaufgabe3;

public interface Algorithm {
    void encrypt(String s);
    void decrypt(String s);
}
